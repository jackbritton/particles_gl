CC     = gcc
CFLAGS = -Wall
LFLAGS = -lSDL2 -lGL -lGLEW -lm
OBJS   = main.o gl_common.o Contig_pool.o
TARGET = particles_gl

all : $(TARGET) run

$(TARGET) : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o $(TARGET)

run : $(TARGET)
	./$(TARGET)

clean :
	rm $(TARGET) $(OBJS)
