#ifndef CONTIG_POOL_H
#define CONTIG_POOL_H

// Contig_pool keeps memory contiguous
// at the cost of swapping memory when dealloc is called.
// This means that pointers break.

struct Contig_pool {
    void *data;
    int max,
        count,
        cell_size;
};

int Contig_pool_make(struct Contig_pool *pool, int cell_size, int max);
void Contig_pool_destroy(struct Contig_pool *pool);

void *Contig_pool_alloc(struct Contig_pool *pool);
int Contig_pool_free(struct Contig_pool *pool, void *ptr);

#endif
