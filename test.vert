#version 130

in vec2 point;
in vec2 uv_vert;

uniform vec2 scale;
uniform vec2 translate;

void main() {
    gl_Position = vec4(scale*(point + translate), 0, 1);
}
