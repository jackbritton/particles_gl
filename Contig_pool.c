#include "Contig_pool.h"

#include <stdlib.h>
#include <string.h>

int Contig_pool_make(struct Contig_pool *pool, int cell_size, int max) {
    pool->data = malloc(cell_size*max);
    if (pool->data == NULL)
        return 1;
    pool->max = max;
    pool->count = 0;
    pool->cell_size = cell_size;
    return 0;
}
void Contig_pool_destroy(struct Contig_pool *pool) {
    free(pool->data);
}
void *Contig_pool_alloc(struct Contig_pool *pool) {
    if (pool->count == pool->max)
        return NULL;
    pool->count++;
    return pool->data + (pool->count-1)*pool->cell_size;
}
int Contig_pool_free(struct Contig_pool *pool, void *ptr) {
    void *end;
    // Exit if pointer is invalid.
    if (ptr < pool->data)
        return 1;
    end = pool->data + (pool->count-1)*pool->cell_size;
    if (ptr > end)
        return 1;
    // Copy the end to the ptr and slice off the tail.
    memcpy(ptr, end, pool->cell_size);
    pool->count--;
    return 0;
}
