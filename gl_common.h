#ifndef GL_COMMON_H
#define GL_COMMON_H

#include <GL/glew.h>
#include <GL/gl.h>

GLuint gl_common_create_program(
    const char *vert_path,
    const char *frag_path,
    char *error_buffer,
    int error_buffer_len
);

void gl_common_rect_to_quad(
    GLfloat out[12],
    float x,
    float y,
    float w,
    float h
);

#endif
