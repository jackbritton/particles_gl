#include "gl_common.h"

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

GLuint gl_common_create_program(
    const char *vert_path,
    const char *frag_path,
    char *error_buffer,
    int error_buffer_len
) {

    FILE *file;
    long file_len;
    char *vert_src,
         *frag_src;

    GLuint vert_shader,
           frag_shader,
           program;
    GLint status;

    // Vertex source.
    file = fopen(vert_path, "r");
    if (file == NULL) {
        strncpy(error_buffer, "failed to open vertex shader", error_buffer_len);
        return 1;
    }
    // Allocate memory for src.
    fseek(file, 0, SEEK_END);
    file_len = ftell(file);
    vert_src = malloc(file_len + 1);
    fseek(file, 0, SEEK_SET);
    // Copy.
    fread((void *) vert_src, file_len, 1, file);
    vert_src[file_len] = 0;
    fclose(file);

    // Fragment source.
    file = fopen(frag_path, "r");
    if (file == NULL) {
        strncpy(error_buffer, "failed to open fragment shader", error_buffer_len);
        return 1;
    }
    // Allocate memory for src.
    fseek(file, 0, SEEK_END);
    file_len = ftell(file);
    frag_src = malloc(file_len + 1);
    fseek(file, 0, SEEK_SET);
    // Copy.
    fread((void *) frag_src, file_len, 1, file);
    frag_src[file_len] = 0;
    fclose(file);

    // Vertex shader.
    vert_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vert_shader, 1, (const GLchar * const *) &vert_src, NULL);
    // Compile.
    glCompileShader(vert_shader);
    glGetShaderiv(vert_shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        glGetShaderInfoLog(vert_shader, error_buffer_len, NULL, error_buffer);
        glDeleteShader(vert_shader);
        return 1;
    }

    // Fragment shader.
    frag_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(frag_shader, 1, (const GLchar * const *) &frag_src, NULL);
    // Compile.
    glCompileShader(frag_shader);
    glGetShaderiv(frag_shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        glGetShaderInfoLog(frag_shader, error_buffer_len, NULL, error_buffer);
        glDeleteShader(vert_shader);
        glDeleteShader(frag_shader);
        return 1;
    }

    // Program.
    program = glCreateProgram();
    glAttachShader(program, vert_shader);
    glAttachShader(program, frag_shader);
    // Link.
    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
        glGetShaderInfoLog(frag_shader, error_buffer_len, NULL, error_buffer);
        glDeleteShader(vert_shader);
        glDeleteShader(frag_shader);
        glDeleteProgram(program);
        return 1;
    }

    // Clean up.
    glDetachShader(program, vert_shader);
    glDetachShader(program, frag_shader);
    glDeleteShader(vert_shader);
    glDeleteShader(frag_shader);
    free(vert_src);
    free(frag_src);

    error_buffer[0] = 0;
    return program;
}

void gl_common_rect_to_quad(
    GLfloat out[12],
    float x,
    float y,
    float w,
    float h
) {
    out[0]  = x;
    out[1]  = y;
    out[2]  = x + w;
    out[3]  = y;
    out[4]  = x + w;
    out[5]  = y + h;
    out[6]  = x;
    out[7]  = y;
    out[8]  = x;
    out[9]  = y + h;
    out[10] = x + w;
    out[11] = y + h;
}

