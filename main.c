#include <stdio.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "Contig_pool.h"
#include "gl_common.h"

struct Vec2 {
    GLfloat x,
            y;
};
// Position is just a pointer - it points to a GPU buffer.
struct Particle {
    struct Vec2 *position;
    struct Vec2  velocity;
};

int main(int argc, char *argv[]) {

    // SDL.
    SDL_Window *window;
    int window_w,
        window_h;
    SDL_GLContext *context;

    // OpenGL.
    GLuint program;
    int error_log_len = 256;
    char error_log[error_log_len];

    // VAO and VBO.
    GLuint vao,
           vbo;

    // Particles.
    struct Contig_pool particles;
    int particles_len;
    // Vertex buffer.
    struct Vec2 *particles_buffer;

    struct Particle *particle;
    int iter;

    // Linear algebra stuff.
    struct Vec2 center,
                pull;
    GLfloat magnitude;

    // Uniforms.
    GLint u_scale,
          u_translate;

    // The camera.
    float scale_x,
          scale_y;
    float translate_x,
          translate_y;

    // Mouse.
    Sint32 mouse_x,
           mouse_y;
    int mouse_down;

    // Loop stuff.
    SDL_Event event;
    Uint32 time_last,
           time_now,
           time_diff;
    int running;

    // Initialize.
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) != 0) {
        printf("SDL_Init: %s\n", SDL_GetError());
        return 1;
    }

    // Window.
    window_w = 640;
    window_h = 480;
    window = SDL_CreateWindow(
        "sdl2",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        window_w,
        window_h,
        SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE
    );
    if (window == NULL) {
        printf("SDL_CreateWindow: %s\n", SDL_GetError());
        return 1;
    }

    // OpenGL.
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    // Context.
    context = SDL_GL_CreateContext(window);
    if (context == NULL) {
        printf("SDL_GL_CreateContext: %s\n", SDL_GetError());
        return 1;
    }
    // Glew.
    glewExperimental = GL_TRUE;
    glewInit();
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glEnable(GL_POINT_SMOOTH);

    // Particles init.
    particles_len = 1e6;
    if (Contig_pool_make(&particles, sizeof(struct Particle), particles_len) != 0) {
        perror("Contig_pool_make\n");
        return 1;
    }
    // Particles buffer.
    particles_buffer = malloc(particles_len*sizeof(struct Vec2));
    if (particles_buffer == NULL) {
        perror("malloc");
        return 1;
    }
    // Point particle positions to buffer.
    for (iter = 0; iter < particles_len; iter++)
        ((struct Particle *)(particles.data + particles.cell_size*iter))->position = &particles_buffer[iter];

    // Shader program.
    program = gl_common_create_program("test.vert", "test.frag", error_log, error_log_len);
    if (error_log[0] != 0) {
        printf("%s\n", error_log);
        return 1;
    }
    glUseProgram(program);

    // VAO.
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    // Vertex buffer.
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(
        GL_ARRAY_BUFFER,
        particles_len*sizeof(struct Vec2),
        particles_buffer,
        GL_STREAM_DRAW
    );
    glVertexAttribPointer(
        0,
        2,
        GL_FLOAT,
        GL_FALSE,
        0,
        0
    );
    // Enable and bind.
    glEnableVertexAttribArray(0);
    glBindAttribLocation(program, 0, "point");

    // Uniforms.
    u_scale = glGetUniformLocation(program, "scale");
    if (u_scale == -1) {
        printf("error: glGetUniformLocation on \"scale\"\n");
        return 1;
    }
    u_translate = glGetUniformLocation(program, "translate");
    if (u_translate == -1) {
        printf("error: glGetUniformLocation on \"translate\"\n");
        return 1;
    }

    // Main loop.
    scale_x = 1;
    scale_y = 1;
    translate_x = 0;
    translate_y = 0;
    running = 1;
    mouse_down = 0;
    time_last = SDL_GetTicks();
    while (running) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = 0;
                break;
            }
            if (event.type == SDL_WINDOWEVENT) {
                if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                    window_w = event.window.data1;
                    window_h = event.window.data2;
                    glViewport(0, 0, window_w, window_h);
                }
                continue;
            }
            if (event.type == SDL_KEYDOWN) {
                if (event.key.keysym.sym == SDLK_MINUS) {
                    scale_x /= 2;
                    scale_y /= 2;
                } else if (event.key.keysym.sym == SDLK_EQUALS) {
                    scale_x *= 2;
                    scale_y *= 2;
                } else if (event.key.keysym.sym == SDLK_w) {
                    translate_y -= 0.1/scale_y;
                } else if (event.key.keysym.sym == SDLK_s) {
                    translate_y += 0.1/scale_y;
                } else if (event.key.keysym.sym == SDLK_a) {
                    translate_x += 0.1/scale_x;
                } else if (event.key.keysym.sym == SDLK_d) {
                    translate_x -= 0.1/scale_x;
                } else if (event.key.keysym.sym == SDLK_l) {
                    translate_x = -center.x;
                    translate_y = -center.y;
                } else if (event.key.keysym.sym == SDLK_k) {
                    particles.count = 0;
                }
                continue;
            }
            if (event.type == SDL_MOUSEMOTION) {
                mouse_x = event.motion.x;
                mouse_y = event.motion.y;
                continue;
            }
            if (event.type == SDL_MOUSEBUTTONDOWN) {
                mouse_down = 1;
                continue;
            }
            if (event.type == SDL_MOUSEBUTTONUP) {
                mouse_down = 0;
                continue;
            }
        }

        // Get time_diff;
        time_now = SDL_GetTicks();
        time_diff = time_now - time_last;

        // Add particles with mousedown.
        if (mouse_down) {
            for (iter = 0; iter < 100; iter++) {
                particle = Contig_pool_alloc(&particles);
                if (particle == NULL)
                    break;
                particle->position->x = ((float)2*mouse_x/window_w - 1)/scale_x - translate_x;
                particle->position->y = -((float)2*mouse_y/window_h - 1)/scale_y - translate_y;
                particle->velocity.x = 0.2*(2*((float)rand() / RAND_MAX) - 1);
                particle->velocity.y = 0.2*(2*((float)rand() / RAND_MAX) - 1);
            }
        }

        // Center of gravity.
        center.x = 0;
        center.y = 0;
        for (
            particle = particles.data;
            particle != (struct Particle *)particles.data + particles.count;
            particle++
        ) {
            center.x += particle->position->x;
            center.y += particle->position->y;
        }
        center.x /= particles.count;
        center.y /= particles.count;

        for (
            particle = particles.data;
            particle != (struct Particle *)particles.data + particles.count;
            particle++
        ) {
            // Normalized vector to center of gravity.
            pull.x = center.x - particle->position->x;
            pull.y = center.y - particle->position->y;
            magnitude = sqrt(pull.x*pull.x + pull.y*pull.y);
            if (magnitude == 0) {
                pull.x = 0;
                pull.y = 0;
            } else {
                pull.x /= magnitude;
                pull.y /= magnitude;
            }

            // Apply pull to velocity.
            particle->velocity.x  += pull.x*time_diff/1000;
            particle->velocity.y  += pull.y*time_diff/1000;
            particle->position->x += particle->velocity.x*time_diff/1000;
            particle->position->y += particle->velocity.y*time_diff/1000;
        }

        // Buffer data.
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferSubData(GL_ARRAY_BUFFER, 0, particles.count*sizeof(struct Vec2), particles_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // Draw.
        glClear(GL_COLOR_BUFFER_BIT);
        glBindVertexArray(vao);
        glUniform2f(u_scale, scale_x, scale_y);
        glUniform2f(u_translate, translate_x, translate_y);
        glPointSize(5*scale_x);
        glDrawArrays(GL_POINTS, 0, particles.count);
        glBindVertexArray(0);

        // Swap.
        SDL_GL_SwapWindow(window);

        // Delay for the remainder of the frame.
        time_last = time_now;
        time_diff = SDL_GetTicks() - time_last;
        if (time_diff >= 1000/60)
            continue;
        SDL_Delay(1000/60 - time_diff);
    }

    // Clean up.
    free(particles_buffer);
    Contig_pool_destroy(&particles);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
